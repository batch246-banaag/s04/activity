

from abc import ABC, abstractclassmethod

class Animal(ABC):

    @abstractclassmethod
    def eat(self, eat):
        pass

    @abstractclassmethod
    def make_sound(self, sound):
        pass

    @abstractclassmethod
    def call(self, call):
        pass



class Dog(Animal):

	def __init__(self, name, breed, age):
		super().__init__() # super() can be used to invoke immediate parent class constructor
		self._name = name
		self._breed = breed
		self._age = age
		
    # SETTERS
	def set_name(self, name):
		self._name = name
	def set_breed(self, breed):
		self._breed = breed
	def set_age(self, age):
		self._age = age
		
	#GETTERS
	def get_name(self):
		print(f"My pet's name is {self._name}")
	def get_breed(self):
		print(f"My pet's breed is {self._breed}")
	def get_age(self):
		print(f"My pet's age is {self._age}")
		
	#
	def eat(self):
		print("Eat steak")
	def make_sound(self):
		print("woof")
	def call(self):
	    print(f'Here {self._name}!')
		

dog = Dog("Lettuce", "Apso", 7)
dog.eat()
dog.make_sound()
dog.call()
dog.get_name()
dog.get_breed()
dog.get_age()


class Cat(Animal):

	def __init__(self, name, breed, age):
		super().__init__() # super() can be used to invoke immediate parent class constructor
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name
	def set_breed(self, breed):
		self._breed = breed
	def set_age(self, age):
		self._age = age
		
    #GETTERS
	def get_name(self):
		print(f"My pet's name is {self._name}")
	def get_breed(self):
		print(f"My pet's breed is {self._breed}")
	def get_age(self):
		print(f"My pet's age is {self._age}")
		
	#
	def eat(self):
		print("Eat tuna")
	def make_sound(self):
		print("meow")
	def call(self):
	    print(f'Here {self._name}!')
		

cat = Cat("Ooma", "American Shorthair", 5)
cat.eat()
cat.make_sound()
cat.call()
cat.get_name()
cat.get_breed()
cat.get_age()




		


		
